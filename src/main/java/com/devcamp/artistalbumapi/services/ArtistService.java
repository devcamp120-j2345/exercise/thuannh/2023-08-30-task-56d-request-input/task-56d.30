package com.devcamp.artistalbumapi.services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.artistalbumapi.models.Artist;
@Service
public class ArtistService extends AlbumService{
    public ArrayList<Artist> artistList(){
        ArrayList<Artist> artistList = new ArrayList<>();
        Artist artist1 = new Artist(100,"Maria", albumListAuthor1());
        Artist artist2 = new Artist(200,"Chopin", albumListAuthor2());
        Artist artist3 = new Artist(300,"Son Tung", albumListAuthor3());
        Artist artist4 = new Artist(400,"Eminem", albumListAuthor4());
        Artist artist5 = new Artist(500,"Justin", albumListAuthor5());
        Artist artist6 = new Artist(600,"Snopp", albumListAuthor6());
        artistList.add(artist1);
        artistList.add(artist2);
        artistList.add(artist3);
        artistList.add(artist4);
        artistList.add(artist5);
        artistList.add(artist6);
        return artistList;
    }
    public Artist getArtistByIndex(int index){

        ArrayList<Artist> allArtist = artistList();

        Artist artistByIndex = new Artist();

        if(index >= 0 && index < allArtist.size()){
            artistByIndex = allArtist.get(index);
        }
        
        return artistByIndex;
    }
}
